package com.example.bk.disaster;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.bk.disaster.models.Family;

public class AddFamily extends AppCompatActivity {

    EditText name, phone;
    Spinner relation;
    CardView save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_family);
        Toolbar toolbar = findViewById(R.id.addfamily_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        name = findViewById(R.id.editText_name);
        phone = findViewById(R.id.editText_phone);
        save = findViewById(R.id.family_btn_save);

        String[] relationType = new String[]{"Son", "Wife", "Husband",  "Daughter", "Brother", "Sister", "Other"};
        relation = findViewById(R.id.spinner_relation);
        ArrayAdapter relationListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, relationType);

        relation.setAdapter(relationListAdapter);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().isEmpty()){
                    Toast.makeText(AddFamily.this, "Name should not be empty", Toast.LENGTH_SHORT).show();
                }else if (phone.getText().toString().isEmpty()){
                    Toast.makeText(AddFamily.this, "phone Should not be empty", Toast.LENGTH_SHORT).show();
                }else{
                    addFamilyMember();
                }


            }
        });

    }
    private void addFamilyMember(){
        Family family = new Family();
        family.setName(name.getText().toString());
        family.setPhone(phone.getText().toString());
        family.setRelation(relation.getSelectedItem().toString());
        family.save();

        Intent intent = new Intent(AddFamily.this, Families.class);
        startActivity(intent);
        finish();
    }
}
