package com.example.bk.disaster.models;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

public class Todo  {
    private String title, detail;

    public Todo() {

    }

    public Todo(String title, String detail) {
        this.title = title;
        this.detail = detail;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
