package com.example.bk.disaster;

import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bk.disaster.models.Family;
import com.example.bk.disaster.models.Todo;

import java.util.List;

/**
 * Created by bk on 10/20/2018.
 */

public class FamilyAdapter extends RecyclerView.Adapter<FamilyAdapter.MyViewHolder> {

private List<Family> familyList;

public class MyViewHolder extends RecyclerView.ViewHolder {
    public TextView title, detail;
    TextView editTextView;
    TextView deleteTextView;

    public MyViewHolder(View view) {
        super(view);
        title = (TextView) view.findViewById(R.id.todo_title);
        detail = (TextView) view.findViewById(R.id.todo_detail);
        this.editTextView = itemView.findViewById(R.id.edit_text_edit_button);
        this.deleteTextView = itemView.findViewById(R.id.edit_text_delete_button);


    }
}


    public FamilyAdapter(List<Family> familyList) {
        this.familyList = familyList;
    }

    @Override
    public FamilyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.todo_layout, parent, false);

        return new FamilyAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FamilyAdapter.MyViewHolder holder, int position) {
        final Family family = familyList.get(position);
        holder.title.setText(family.getName());
        holder.detail.setText(family.getPhone());
        TextView editTextView = holder.editTextView;
        TextView deleteTextView = holder.deleteTextView;
        deleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                family.delete();
            }
        });



    }


    @Override
    public int getItemCount() {
        return familyList.size();
    }
}

