package com.example.bk.disaster;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;

import com.example.bk.disaster.helpers.SharedPrefs;

public class Splash extends AppCompatActivity {

    private SharedPrefs prefs;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int SPLASH_DISPLAY_LENGTH = 1000;
        setContentView(R.layout.activity_splash);

            new Handler().postDelayed(new Runnable(){
                @Override
                public void run() {

                    prefs = new SharedPrefs(getApplicationContext());
                    if (prefs.getUserName() == null || prefs.getUserPhone() == null){
                        Intent mainIntent = new Intent(Splash.this,Login.class);
                        startActivity(mainIntent);
                        finish();
                    }
                    else{
                        Intent mainIntent = new Intent(Splash.this,Home.class);
                        startActivity(mainIntent);
                        finish();
                    }

                }
            }, SPLASH_DISPLAY_LENGTH);
    }
}
