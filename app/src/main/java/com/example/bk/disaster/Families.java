package com.example.bk.disaster;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.bk.disaster.models.Family;
import com.example.bk.disaster.models.Todo;
import com.orm.SugarContext;

import java.util.ArrayList;
import java.util.List;

public class Families extends AppCompatActivity {

    private RecyclerView recyclerView;
    private FamilyAdapter mAdapter;
    FloatingActionButton fab;
    Context context;
    List<Family> familyList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_families);
        Toolbar toolbar = findViewById(R.id.families_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        SugarContext.init(getApplicationContext());

        Family family = new Family();

        familyList = family.listAll(Family.class);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new FamilyAdapter(familyList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        fab = findViewById(R.id.floatingActionButton_family);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Families.this, AddFamily.class);
                startActivity(intent);
            }
        });

        prepareFamilyData();
    }

    private void prepareFamilyData() {



        if (familyList.size() == 0) {

            Toast.makeText(getApplicationContext(), "empty", Toast.LENGTH_SHORT).show();

        }else{
            recyclerView.setAdapter(mAdapter);
        }
        mAdapter.notifyDataSetChanged();


    }

    public void removeIncomeSourceFromList(int position) {
        familyList.remove(position);
        mAdapter.notifyDataSetChanged();
    }
}
