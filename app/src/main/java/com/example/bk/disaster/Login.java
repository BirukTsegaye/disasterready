package com.example.bk.disaster;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.bk.disaster.helpers.SharedPrefs;

public class Login extends AppCompatActivity {

    AppCompatEditText name,phone;
    CardView login;
    SharedPrefs prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        name  = findViewById(R.id.editText_name);
        phone = findViewById(R.id.EditText_Phone);
        login = findViewById(R.id.Btn_login);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                prefs.saveUserName(name.getText().toString());
                prefs.savePhone(phone.getText().toString());

                Intent intent = new Intent(Login.this, Home.class);
                startActivity(intent);

            }
        });
    }
}
