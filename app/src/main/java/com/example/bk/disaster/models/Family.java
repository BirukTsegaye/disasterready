package com.example.bk.disaster.models;

import com.orm.SugarRecord;

/**
 * Created by bk on 10/19/2018.
 */

public class Family extends SugarRecord{
    private String name, relation;
    private String phone;

    public Family(){
        super();
    }
    public Family(String name, String relation, String phone) {
        this.name = name;
        this.relation = relation;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
