package com.example.bk.disaster.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by bk on 7/23/2018.
 */

public class SharedPrefs {

    private Context context;

    public SharedPrefs(Context context) {
        this.context = context;
    }

    public void saveUserName(String name) {
        SharedPreferences.Editor editor = context.getSharedPreferences("NAME", Context.MODE_PRIVATE).edit();
        editor.putString("NAME", name);
        editor.apply();
    }

    public void savePhone(String phone) {
        SharedPreferences.Editor editor = context.getSharedPreferences("PHONE", Context.MODE_PRIVATE).edit();
        editor.putString("PHONE", phone);
        editor.apply();
    }
    public String getUserPhone() {
        SharedPreferences prefs = context.getSharedPreferences("PHONE", Context.MODE_PRIVATE);
        return prefs.getString("PHONE", "");
    }

    public void setAsFirstTime(boolean firstTime) {
        SharedPreferences.Editor editor = context.getSharedPreferences("FIRSTTIME", Context.MODE_PRIVATE).edit();
        editor.putBoolean("FIRSTTIME", true);
        editor.apply();
    }



    public boolean isFirstTime() {
        SharedPreferences editor = context.getSharedPreferences("FIRSTTIME", Context.MODE_PRIVATE);
        return context.getSharedPreferences("FIRSTTIME", Context.MODE_PRIVATE).getBoolean("FIRSTTIME", true);
    }

    public String getUserName() {
        SharedPreferences prefs = context.getSharedPreferences("NAME", Context.MODE_PRIVATE);
        return prefs.getString("NAME", "");
    }


}
