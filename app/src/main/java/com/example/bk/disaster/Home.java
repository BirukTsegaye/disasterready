package com.example.bk.disaster;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.telephony.SmsManager;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.bk.disaster.models.Family;

import java.util.List;

import static android.widget.Toast.LENGTH_LONG;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    CardView disaster;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Family> families = Family.listAll(Family.class);
                for (int i =0; i<families.size(); i++){
                    Family family = families.get(i);
                    String phone = family.getPhone();

                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phone, null, "Disaster is Happening", null, null);
                    Toast.makeText(getApplicationContext(), "SMS sent.", LENGTH_LONG).show();
                }
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        disaster = findViewById(R.id.cardView_disaster);
        disaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, DisasterTyps.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            Intent intent = new Intent(Home.this, Login.class);
            startActivity(intent);

        } else if (id == R.id.nav_todo) {
            Intent intent = new Intent(Home.this, TodoList.class);
            startActivity(intent);

        } else if (id == R.id.nav_familiy) {
            Intent intent = new Intent(Home.this, Families.class);
            startActivity(intent);

        } else if (id == R.id.nav_visualization) {

            Intent intent = new Intent(Home.this, Families.class);
            startActivity(intent);


        } else if (id == R.id.nav_precautions) {

            Intent intent = new Intent(Home.this, Precautions.class);
            startActivity(intent);
        }else if (id == R.id.nav_share) {
            shareWith();

        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(Home.this, about.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {

            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(new ContextThemeWrapper(Home.this, R.style.Theme_AppCompat_Dialog));
            builder.setMessage("Are you sure you want to logout?")
                    .setTitle("Warning")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {


                            Intent intent = new Intent(Home.this, Login.class);
                            startActivity(intent);
                            finish();

                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }).create();
            builder.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void shareWith() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "share with";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "sharing");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));

    }
}

