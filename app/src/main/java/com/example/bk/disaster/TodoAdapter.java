package com.example.bk.disaster;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bk.disaster.models.Todo;

import java.util.List;

/**
 * Created by bk on 10/19/2018.
 */
public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.MyViewHolder> {

    private List<Todo> todoList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, detail;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.todo_title);
            detail = (TextView) view.findViewById(R.id.todo_detail);
        }
    }


    public TodoAdapter(List<Todo> todoList) {
        this.todoList = todoList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.todo_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Todo todo = todoList.get(position);
        holder.title.setText(todo.getTitle());
        holder.detail.setText(todo.getDetail());
    }

    @Override
    public int getItemCount() {
        return todoList.size();
    }
}