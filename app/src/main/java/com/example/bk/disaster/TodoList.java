package com.example.bk.disaster;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.bk.disaster.models.Todo;

import java.util.ArrayList;
import java.util.List;

public class TodoList extends AppCompatActivity {

        private List<Todo> todoList = new ArrayList<>();
        private RecyclerView recyclerView;
        private TodoAdapter mAdapter;
        FloatingActionButton fab;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_todo_list);

            Toolbar toolbar = findViewById(R.id.todo_toolbar);
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onBackPressed();
                }
            });

            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

            mAdapter = new TodoAdapter(todoList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
            fab = findViewById(R.id.floatingActionButton);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TodoList.this, NewTodo.class);
                    startActivity(intent);
                }
            });

            prepareMovieData();
        }

        private void prepareMovieData() {
            Todo todo = new Todo("Mad Max: Fury Road","2015");
            todoList.add(todo);

            todo = new Todo("Inside Out",  "2015");
            todoList.add(todo);

            todo = new Todo("Star Wars: Episode VII - The Force Awakens", "2015");
            todoList.add(todo);

            todo = new Todo("Shaun the Sheep",  "2015");
            todoList.add(todo);

            todo = new Todo("Guardians of the Galaxy","2014");
            todoList.add(todo);

            mAdapter.notifyDataSetChanged();
        }
    }

